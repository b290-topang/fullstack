// fetch() method in Javascript is used to send request in the server and load the received response in the webpage/s. The request and response is in JSON format

/*
	Syntax: 
		fetch("url", { options })
			url - this is the address which the request is to be made and source where the response will come from (endpoint)
			options - array or properties that contains the HTTP method, body of request and headers
*/

// Get Post Data / Retrieve/Read Function
	fetch("https://jsonplaceholder.typicode.com/posts")
		.then(response => response.json())
			.then(data => showPosts(data));

// View Post - to display each post from JSON placeholder

	const showPosts = (posts) => {
		//Create a variable that will contain all the posts
		let postEntries = "";

		posts.forEach(post => {
			postEntries += `
				<div id="post-${post.id}">
					<h3 id="post-title-${post.id}">${post.title}</h3>
					<p id="post-body-${post.id}">${post.body}</p>

					<button onClick="editPost('${post.id}')">Edit</button>
					<button onClick="deletePost('${post.id}')">Delete</button>
				</div>

			`
		})

		// To check what is stored in the postEntries variables.
		 //console.log(postEntries);

		// To assign the value of postEntries to the element with "div-post-entries" id.
		document.querySelector("#div-post-entries").innerHTML = postEntries
	}

/*
	Mini-Activity:
		Retrieve a single post from JSON API and print in in the console.
*/

/*	fetch("https://jsonplaceholder.typicode.com/posts/1")
		.then(response => response.json())
			.then(data => console.log(data))

	fetch("https://jsonplaceholder.typicode.com/posts/2")
		.then(response => response.json())
			.then(data => console.log(data))
*/

// Post Data / Create Function
	document.querySelector("#form-add-post").addEventListener("submit", event => {
		event.preventDefault();

		fetch("https://jsonplaceholder.typicode.com/posts", {
			method: "POST",
			body: JSON.stringify({ 
				title: document.querySelector("#txt-title").value,
				body: document.querySelector("#txt-body").value,
				userId: 290
			}),

			headers: {
				"Content-Type": "application/json"
			}
		}).then(response => response.json())
			.then(data => { 
				console.log(data);
				alert("Post successfully added!")
			})

		document.querySelector("#txt-title").value = null;
		document.querySelector("#txt-body").value = null;
	})

// Edit Post / Edit Button
	const editPost = (id) => {

		let title = document.querySelector(`#post-title-${id}`).innerHTML;
		let body = document.querySelector(`#post-body-${id}`).innerHTML;

		document.querySelector("#txt-edit-id").value = id;
		document.querySelector("#txt-edit-title").value = title;
		document.querySelector("#txt-edit-body").value = body

		document.querySelector("#btn-submit-update").removeAttribute("disabled");
	}

// Update Post Data / PUT Method
	document.querySelector("#form-edit-post").addEventListener("submit", event => {
		event.preventDefault();

		let id = document.querySelector("#txt-edit-id").value;
		fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
			method: "PUT",
			body: JSON.stringify({ 
				id: id,
				title: document.querySelector("#txt-edit-title").value,
				body: document.querySelector("#txt-edit-body").value,
				userId: 290
			}), 

			headers: {
				"Content-Type": "application/json"
			}
		}).then(response => response.json())
			.then(data => {
				console.log(data);
				alert("Post successfully updated!")
			})

		document.querySelector("#txt-edit-title").value = null;
		document.querySelector("#txt-edit-body").value = null;

		document.querySelector("#btn-submit-update").setAttribute("disabled", true);	
	})

// Delete Button

	const deletePost = (id) => {
		event.preventDefault();
	
		fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
			method: "DELETE",
			body: JSON.stringify({ 
				id: id,
			}), 

			headers: {
				"Content-Type": "application/json"
			}
		}).then(response => response.json())
			.then(data => {
				alert("Post successfully deleted!")
			})

		let element = document.querySelector(`#post-${id}`);
		element.remove();
	}
