// mock data will contain all the posts
	let posts = [];
	// will be the id
	let count = 1;

	// CREATE (add post)
	document.querySelector("#form-add-post").addEventListener("submit", event => {
		event.preventDefault();

		posts.push({
			id: count,
			title: document.querySelector("#txt-title").value,
			body: document.querySelector("#txt-body").value
		})

		count++;

		showPosts(posts);
		// alert("Successfully added!")

		document.querySelector("#txt-title").value = null;
		document.querySelector("#txt-body").value = null;
	})

	//READ POSTS

	const showPosts = (posts) => {
		//Create a variable that will contain all the posts
		let postEntries = "";

		posts.forEach(post => {
			postEntries += `
				<div id="post-${post.id}">
					<h3 id="post-title-${post.id}">${post.title}</h3>
					<p id="post-body-${post.id}">${post.body}</p>

					<button onClick="editPost('${post.id}')">Edit</button>
					<button onClick="deletePost('${post.id}')">Delete</button>
				</div>

			`
		})

		// To check what is stored in the postEntries variables.
		 //console.log(postEntries);

		// To assign the value of postEntries to the element with "div-post-entries" id.
		document.querySelector("#div-post-entries").innerHTML = postEntries
	}

	// EDIT BUTTON

	// Edit Post / Edit Button
	const editPost = (id) => {

		let title = document.querySelector(`#post-title-${id}`).innerHTML;
		let body = document.querySelector(`#post-body-${id}`).innerHTML;

		document.querySelector("#txt-edit-id").value = id;
		document.querySelector("#txt-edit-title").value = title;
		document.querySelector("#txt-edit-body").value = body

		document.querySelector("#btn-submit-update").removeAttribute("disabled");
	}

	// UPDATE BUTTON

	document.querySelector("#form-edit-post").addEventListener("submit", event => {
		event.preventDefault();

		for (let index = 0; index < posts.length; index++) {
			if(posts[index].id.toString() === document.querySelector("#txt-edit-id").value) {
				posts[index].title = document.querySelector("#txt-edit-title").value;
				posts[index].body = document.querySelector("#txt-edit-body").value;

				showPosts(posts);
				alert("Successfully updated!");

				break;
			}
		}

		document.querySelector("#txt-edit-title").value = null;
		document.querySelector("#txt-edit-body").value = null;

		document.querySelector("#btn-submit-update").setAttribute("disabled", true);		
	})


	