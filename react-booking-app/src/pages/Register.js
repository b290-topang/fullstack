import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Register() {

	// State hooks to store the values of the input fields
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [mobile, setMobile] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);

    const navigate = useNavigate();

    // Check if values are successfuly binded
    // console.log(email);
    // console.log(password1);
    // console.log(password2);

    const { user, setUser } = useContext(UserContext)
    localStorage.setItem("email", email);

    // Function to simulate user registration
    function registerUser(e) {

    	// Prevents page redirection via form submission
    	 e.preventDefault();

    	

    	fetch (`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
    		method: "POST",
    		headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email: email
            })
    	})
    	.then(res => res.json())
    	.then(data => {
    		
    		if (data === true) {
    			Swal.fire({
                    icon: 'error',
                    title: 'Duplicate email found',
                    text: 'Please provide a different email.',
                })
    			

    		} else {

    			Swal.fire({
                    icon: 'success',
                    title: 'Registration successful!',
                    text: 'Welcome to Zuitt!',
                })	

            	fetch (`${process.env.REACT_APP_API_URL}/users/register`, {
					method: "POST",
					headers: {
			            "Content-Type": "application/json"
			        },
			        body: JSON.stringify({
			           	firstName: firstName,
			           	lastName: lastName,
			           	email: email,
			           	mobileNo: mobile,
			           	password: password1
			        })
				})
				.then(res => res.json())
				.then(data => { return true })

				navigate("/login"); 
    		}
    	})

    	
    	



    	// Clear input fields
    	 setFirstName('');
    	 setLastName('');
    	 setEmail('');
    	 setMobile('');
    	 setPassword1('');
    	 setPassword2('');
    }

    useEffect(() => {
    	// Validation to enable submit button when all fields are populated and both passwords match
    	if((firstName !== "" && lastName !== "" && email !== "" && mobile !== "" && password1 !== "" && password2 !== "" ) && (password1 === password2) && (mobile.length === 11)){
    		setIsActive(true);
    	} else {
    		setIsActive(false);
    	}
    }, [firstName, lastName, email, mobile, password1, password2]);

	return(

		(user.id) ?
				<Navigate to="/courses" />			
			:
				<Form onSubmit={(e) => registerUser(e)}>
					{/* First Name */}
					<Form.Group controlId="userFirstName">
						<Form.Label>First Name</Form.Label>
						<Form.Control
							type="text"
							placeholder="Enter first name"
							value={ firstName }
							onChange={e => setFirstName(e.target.value)}
							required
						 />
					</Form.Group>

					{/* Last Name */}
					<Form.Group controlId="userLastName">
						<Form.Label>Last Name</Form.Label>
						<Form.Control
							type="text"
							placeholder="Enter last name"
							value={ lastName }
							onChange={e => setLastName(e.target.value)}
							required
						 />
					</Form.Group>

					{/* Email */}
					<Form.Group controlId="userEmail">
						<Form.Label>Email Address</Form.Label>
						<Form.Control
							type="email"
							placeholder="Enter an email"
							value={ email }
							onChange={e => setEmail(e.target.value)}
							required
						 />
						 <Form.Text className="text-muted">We'll never share your email with anyone.</Form.Text>
					</Form.Group>

					{/* Mobile Number */}
					<Form.Group controlId="userMobile">
						<Form.Label>Mobile Number</Form.Label>
						<Form.Control
							type="text"
							placeholder="Enter Mobile Number"
							value={ mobile }
							onChange={e => setMobile(e.target.value)}
							required
						 />
					</Form.Group>

					{/* Password */}
					<Form.Group controlId="password1">
						<Form.Label>Password</Form.Label>
						<Form.Control
							type="password"
							placeholder="Password"
							value={ password1 }
							onChange={e => setPassword1(e.target.value)}
							required
						 />
					</Form.Group>

					{/* Confirm Password */}
					<Form.Group controlId="password2">
						<Form.Label>Confirm Password</Form.Label>
						<Form.Control
							type="password"
							placeholder="Verify Password"
							value={ password2 }
							onChange={e => setPassword2(e.target.value)}
							required
						 />
					</Form.Group>
					
					{/*conditional rendering for submit button based on isActive state*/}
					{
						isActive ?
							<Button variant="primary" type="submit" id="submitBtn" className="mt-2" to="/login">Register</Button>
							:
							<Button variant="dark" type="submit" id="submitBtn" className="mt-2" disabled >Register</Button>
					}
				</Form>
	)
}
