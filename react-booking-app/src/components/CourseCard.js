import { useState, useEffect } from 'react';
import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

// create a parameter that will receive the prop from parent component Courses.js
// export default function CourseCard(props) {
// Destructure the props parameter and get the course prop directly from parent component Courses.js
export default function CourseCard({ course }) {

    /*
        props = {
            course : {
                id: "wdc001",
                name: "PHP - Laravel",
                description: "Nostrud velit dolor excepteur ullamco consectetur aliquip tempor. Consectetur occaecat laborum exercitation sint reprehenderit irure nulla mollit. Do dolore sint deserunt quis ut sunt ad nulla est consectetur culpa. Est esse dolore nisi consequat nostrud id nostrud sint sint deserunt dolore.",
                price: 45000,
                onOffer: true
            }
        }
    */

    // Checks to see if the data was successfully passed
    // Every component receives information in a form of an object

    // Destructure the course prop from parent component Courses.js
    const { _id, name, description, price } = course;


    // Use the state hook for this component to be able to store its state
    // States are used to keep track of information related to individual components
    // Syntax
        // const [getter, setter] = useState(initialGetterValue);
   // const [ count, setCount ] = useState(0);
    // Using the state hook returns an array with the first element being a value and the second element as a function that's used to change the value of the first element

    // Use state hook for getting and setting the seats for this course
    
    /*const [seats, setSeats] = useState(30);
    const [isOpen, setIsOpen] = useState(true);

    useEffect(() => {
        if(seats === 0){
            // alert("No more seats available");
            setIsOpen(false)
        }
    }, [seats])
    */

    /*function enroll(){
        // if (seats > 0) {
            setCount(count + 1);
            // console.log('Enrollees: ' + count);
            setSeats(seats - 1);
            // console.log('Seats: ' + seats);
        // } //else {
            alert("No more seats available");
        };
    }*/

    

    return (
        <Card className="mt-3">
            <Card.Body>
                {/*Dot notation if props will receive by a parameter*/}
                {/*<Card.Title>{ props.course.name }</Card.Title>*/}
                {/*Dot notation if props parameter is destructured. It will use the props name declared in the parent component*/}
                {/*<Card.Title>{ course.name }</Card.Title>*/}
                {/*Property name of destructured course prop from parent component*/}
                <Card.Title>{ name }</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                {/*<Card.Text>{ props.course.description }</Card.Text>*/}
                {/*<Card.Text>{ course.description }</Card.Text>*/}
                <Card.Text>{ description }</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                {/*<Card.Text>PhP { props.course.price }</Card.Text>*/}
                {/*<Card.Text>PhP { course.price }</Card.Text>*/}
                <Card.Text>PhP { price }</Card.Text>
                {/*<Card.Text>Enrollees: { count }</Card.Text>
                <Card.Text>Seats: { seats }</Card.Text>


                {
                    isOpen ?
                        <Button variant="primary" onClick={ enroll }>Enroll</Button>   
                        :
                        <Button variant="primary" onClick={ enroll } disabled>Enroll</Button>   
                } */} 

                <Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>            
            </Card.Body>
        </Card>
    )
}
