let collection = [];
let elements = [];
let head = 0;
let tail = 0;

// Write the queue functions below.
function print() {
	return collection;
}

function enqueue(element) {
	collection[tail] = element;
	tail++; 

	return collection;
}

function dequeue() {
	const item = collection[head];
	collection[head]--;
	head++;

	return collection;

	
	/*for (let i=0; i < collection.length - 1; i++) {
		collection[i] = collection[i + 1];
		collection--;

		return collection;
	}*/

	
}

function front() {
	return collection[head];
}

function size() {
	return tail - head;
}

function isEmpty() {
	let result = tail - head;

	if (result === 0) {
		return true;
	} else {
		return false;
	}
}

module.exports = {
	print, enqueue, dequeue, front, size, isEmpty
};